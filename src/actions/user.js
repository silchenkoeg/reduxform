import {UPDATE_USER, RESET_USER, GET_USER} from './actionTypes';

export const updateUser = data => ({
  type: UPDATE_USER,
  data
});

export const resetUser = () => ({
  type: RESET_USER,
});

export const getUser = data => ({
  type: GET_USER,
  data
});