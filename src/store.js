import {createStore, combineReducers} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import { reducer as formReducer } from 'redux-form'
import userReducer from './reducers/user';

const store = createStore(
  combineReducers({
    user: userReducer,
    form: formReducer,
  }),
  composeWithDevTools(),
);

export default store;