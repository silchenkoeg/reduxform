import React, {PureComponent} from 'react';
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import {Field} from "redux-form";
import Input from "../commons/Input";
import Button from "@material-ui/core/Button";

export default class SimpleForm extends PureComponent {

  showResult = value => {
    alert('login');
    this.props.reset();
  };

  render() {
    const { classes, handleSubmit, reset, pristine, invalid, submitting  } = this.props;
    return (
      <div className={classes.rootContainer}>
        <Paper className={classes.root}>
          <Typography variant="h5" component="h3">
            Simple Redux-Form
          </Typography>
          <form onSubmit={handleSubmit(this.showResult)}>
            <div className={classes.container}>
              <Field
                name='login'
                component={Input}
                label='Login'
              />
              <Field
                name='password'
                component={Input}
                label='Password'
              />
            </div>
            <Button type="submit" variant="outlined" color="primary" disabled={invalid|| submitting || pristine}
                    className={classes.button}>
              Submit
            </Button>
            <Button onClick={reset} variant="outlined" color="primary" className={classes.button}>
              Reset Form
            </Button>

          </form>
        </Paper>
      </div>
    )
  }
}