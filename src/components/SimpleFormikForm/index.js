import SimpleFormikForm from './SimpleFormikForm';
import {withStyles} from "@material-ui/core";
import { withFormik } from 'formik';

import styles from './styles';
import { validateLogin as validate  } from '../../utils/validation';

const SimpleFormikFormHOC = withFormik({
    displayName: 'SimpleFormikForm',
    mapPropsToValues: () => ({ login: '', password: ''  }),
    handleSubmit: (values, props) => {
        alert('Login !');
        props.resetForm();
    },
    validate,
})(SimpleFormikForm);

export default withStyles(styles)(SimpleFormikFormHOC);