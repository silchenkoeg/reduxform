export const validateUser = values => {
  console.log(values);
  const errors = {}
  if (!values.name) {
    errors.name = 'Required'
  } else if (values.name.length > 15) {
    errors.name = 'Must be 15 characters or less'
  }
  if (!values.username) {
    errors.username = 'Required'
  } else if (values.username.length > 15) {
    errors.username = 'Must be 15 characters or less'
  }
  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  return errors
};

export const validateLogin = values => {
  const errors = {}
  if (!values.login) {
    errors.login = 'Required'
  } else if (values.login.length > 15) {
    errors.login = 'Must be 15 characters or less'
  } else if (values.login.length < 4) {
    errors.login = 'Must be more than 4'
  }
  if (!values.password) {
    errors.password = 'Required'
  } else if (values.password.length > 15) {
    errors.password = 'Must be 15 characters or less'
  } else if (values.password.length < 4) {
    errors.password = 'Must be more than 4'
  }
  return errors
};

