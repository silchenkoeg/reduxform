import AndancedFormikForm from './AndancedFormikForm';
import {withStyles} from "@material-ui/core";
import {connect} from 'react-redux';

import styles from './styles';
import { getUser, resetUser, updateUser } from '../../actions/user';

const mapStateToProps = ({user}) => ({user});

export default connect(mapStateToProps,{
  getUser, resetUser, updateUser
})(withStyles(styles)(AndancedFormikForm));