import AdvancedForm from './AdvancedForm';

import {connect} from 'react-redux';
import {reduxForm} from 'redux-form'
import {withStyles} from "@material-ui/core";

import styles from './styles';
import { getUser, resetUser, updateUser } from '../../actions/user';
import { validateUser as validate } from '../../utils/validation';

const mapStateToProps = ({user}) => ({ initialValues: user});

const InitializeAdvancedForm = reduxForm({
  form: 'advancedForm',
  enableReinitialize: true,
  validate,
})(AdvancedForm);

export default connect(mapStateToProps, { getUser, resetUser, updateUser })(withStyles(styles)(InitializeAdvancedForm));