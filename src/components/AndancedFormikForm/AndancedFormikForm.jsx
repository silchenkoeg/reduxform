import React, {PureComponent} from 'react';
import Paper from '@material-ui/core/Paper/index';
import Typography from '@material-ui/core/Typography/index';
import Button from "@material-ui/core/Button";
import FormikInput from '../commons/FormikInput';
import {sendRequest} from '../../utils/requests';
import {Formik, Field, Form} from 'formik';
import {validateLogin} from '../../utils/validation';
import * as Yup from 'yup';

const UserSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(10, 'Too Long!')
    .required('Required'),
  username: Yup.string()
    .min(2, 'Too Short!')
    .max(10, 'Too Long!')
    .required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
});


export default class AdvancedForm extends PureComponent {


  getUserData = async () => {
    const user = await fetch('https://jsonplaceholder.typicode.com/users/1').then(res => res.json());
    this.props.getUser(user);
  };

  handleSubmit =  async (values, actions) => {
    try{
      const response = await sendRequest(values);
      this.props.updateUser(values);
    } catch(e) {
      console.log(this.props);
      console.log(actions);
      actions.setFieldError('username', e.error)

    }
  }

  render() {
    const {classes, user, resetUser} = this.props;
    return (
      <div className={classes.rootContainer}>
        <Paper className={classes.root}>
          <Typography variant="h5" component="h3">
            Adnvanced Formik-Form
          </Typography>
          <Formik
            initialValues={user}
            enableReinitialize={true}
            validationSchema={UserSchema}
            onSubmit={this.handleSubmit}
            render={props => (
                <Form onSubmit={props.handleSubmit}>
                  <div className={classes.container}>
                    <Field
                      name='name'
                      component={FormikInput}
                      label='Name'
                    />
                    <Field
                      name='username'
                      component={FormikInput}
                      label='Username'
                    />
                     <Field
                      name='email'
                      component={FormikInput}
                      label='Email'
                    />
                  </div>
                  <Button onClick={this.getUserData} variant="outlined" color="primary"
                          className={classes.button}>
                    Get User
                  </Button>
                  <Button onClick={resetUser} variant="outlined" color="primary"
                          className={classes.button}>
                            Reset User
                  </Button>
                  <Button type="submit" variant="outlined" color="primary"
                          className={classes.button}>
                    Submit
                  </Button>
              
                </Form>
            )}
          />
        </Paper>
      </div>
    );
  }
}