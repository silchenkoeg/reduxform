import React from "react";
import TextField from "@material-ui/core/TextField/index";

const Input = ({
                       input,
                       label,
                       meta: {touched, error},
                       ...custom
                       }) => (
  <TextField
    label={label}
    helperText={touched && error}
    error={touched && !!error}
    autoComplete='off'
    {...input}
    {...custom}
  />
);

export default Input;