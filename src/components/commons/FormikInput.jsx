import React from "react";
import TextField from "@material-ui/core/TextField/index";

const FormikInput = ({
    field: { value, ...fields },
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        {...props}
        {...fields}
        value={value}
        error={(touched[fields.name] && !!errors[fields.name])}
        helperText={touched[fields.name] && errors[fields.name]}
      />
    );
  };

  export default FormikInput;