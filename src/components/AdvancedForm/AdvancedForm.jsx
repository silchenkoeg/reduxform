import React, {PureComponent} from 'react';
import Paper from '@material-ui/core/Paper/index';
import Typography from '@material-ui/core/Typography/index';
import Button from "@material-ui/core/Button";
import { Field, SubmissionError } from 'redux-form'
import Input from '../commons/Input';
import {sendRequest} from '../../utils/requests';

export default class AdvancedForm extends PureComponent {

  getUserData = async () => {
    const user = await fetch('https://jsonplaceholder.typicode.com/users/1').then(res => res.json());
    this.props.getUser(user);
  };

  submitForm = async values => {
    try{
      const res = await sendRequest(values);
      this.props.updateUser(values);
      alert(res.data)
    } catch(e) {
      throw new SubmissionError({
        username: e.error
      });
    }
  };

  handleChangeField = () => {
    this.props.change('name', 'handle change field')
  };



  render() {
    const {classes, pristine, submitting, invalid, handleSubmit} = this.props;
    return (
      <div className={classes.rootContainer}>
        <Paper className={classes.root}>
          <Typography variant="h5" component="h3">
            Advanced Redux-Form
          </Typography>
          <form onSubmit={handleSubmit(this.submitForm)} >
            <div className={classes.container}>
              <Field
                name='name'
                component={Input}
                label='Name'
              />
              <Field
                name='username'
                component={Input}
                label='Username'
              />
              <Field
                name='email'
                component={Input}
                label='Email'
              />
            </div>
            <Button variant="outlined" onClick={this.props.resetUser} color='secondary' className={classes.button}>
              Reset store
            </Button>
            <Button variant="outlined" onClick={this.getUserData} className={classes.button}>
              Get User
            </Button>
            <Button type="submit" variant="outlined" color="primary" disabled={invalid|| submitting || pristine} className={classes.button}>
              Update
            </Button>
            <Button variant="outlined" onClick={this.handleChangeField} className={classes.button}>
              Handle Change
            </Button>

          </form>
        </Paper>
      </div>
    );
  }
}