import {UPDATE_USER, RESET_USER, GET_USER} from '../actions/actionTypes';

const initialState = {
  email: '',
  name: '',
  username:'',
};


export default function (state = initialState, action) {
  switch (action.type) {
    case UPDATE_USER:
      return { ...action.data};
    case GET_USER:
      return { ...action.data};
    case RESET_USER:
      return initialState;
    default:
      return state
  }
}