import SimpleForm from './SimpleForm';

import {reduxForm} from 'redux-form'
import {withStyles} from "@material-ui/core";

import {validateLogin as validate} from "../../utils/validation";
import styles from './styles';


const SimpleFormHOC = reduxForm({
  form: 'simpleForm',
  validate,
})(SimpleForm);


export default withStyles(styles)(SimpleFormHOC) ;