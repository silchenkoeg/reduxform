export default (theme) => ({
    root: {
      padding: theme.spacing(3, 2),
      minWidth: '600px',
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
    },
    button: {
      margin: theme.spacing(1),
    },
    rootContainer: {
      marginRight: '10px',
      marginLeft: '10px',
    }
  })