import React from 'react';
import AdvancedForm from './components/AdvancedForm';
import SimpleForm from './components/SimpleForm';
import SimpleFormikForm from './components/SimpleFormikForm';
import AdvancedFormikForm from './components/AndancedFormikForm';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <SimpleForm /> */}
        {/* <AdvancedForm /> */}
        {/* <SimpleFormikForm /> */}
        <AdvancedFormikForm />
      </header>
    </div>
  );
}

export default App;