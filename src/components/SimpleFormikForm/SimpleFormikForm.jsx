import React, { PureComponent } from 'react';
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Field } from 'formik';

import FormikInput from '../commons/FormikInput';


export default class SimpleFormikForm extends PureComponent {

    render(){
        const { classes, handleSubmit } = this.props;
        return(
            <div className={classes.rootContainer}>
            <Paper className={classes.root}>
              <Typography variant="h5" component="h3">
                Simple Formik-Form
              </Typography>
              <form onSubmit={handleSubmit}>
                <div className={classes.container}>
                  <Field
                    name='login'
                    component={FormikInput}
                    label='Login'
                  />
                  <Field
                    name='password'
                    component={FormikInput}
                    label='Password'
                  />
                </div>
                <Button type="submit" variant="outlined" color="primary"
                        className={classes.button}>
                  Submit
                </Button>
              </form>
            </Paper>
          </div>
        )
    }
}